<?php

$DB_NAME = "uas_catering";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $nama_menu = $_POST['nama_menu'];
    if(empty($nama_menu)){
        $sql = "SELECT m.id_menu, m.nama_menu, concat('http://192.168.43.170/www/cat/images/menu/', m.photosa) as url
            FROM menu m
            ORDER BY nama_menu ASC";
    }else{
        $sql = "SELECT m.id_menu, m.nama_menu, concat('http://192.168.43.170/www/cat/images/menu/', m.photosa) as url
            FROM menu m
            WHERE nama_menu LIKE '%$nama_menu%' 
            ORDER BY nama_menu ASC";
    }
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_menu = array();
        while ($menu = mysqli_fetch_assoc($result)) {
            array_push($data_menu, $menu);
        }
        echo json_encode($data_menu);
    }
}
