<?php

$DB_NAME = "uas_catering";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
    $mode = $_POST['mode'];
    $respon = array(); 
    $respon['kode'] = '000';
    switch($mode){
        case "insert":
            $kd_cat = $_POST['kd_cat'];
            $nama_cat = $_POST['nama_cat'];
            $nama_menu = $_POST['nama_menu'];
            $email = $_POST['email'];
            $nama_pemilik = $_POST['nama_pemilik'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/catering/";

            $sql = "select m.id_menu, p.id_pemilik from menu m, pemilik p where m.nama_menu = '$nama_menu' and p.nama_pemilik = '$nama_pemilik'";
            $result = mysqli_query($conn, $sql);
            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_menu = $data['id_menu'];
                $id_pemilik = $data['id_pemilik'];

                $sql = "insert into catering(kd_cat, nama_cat, id_menu, photosb, email, id_pemilik) values(
                    '$kd_cat','$nama_cat','$id_menu','$file', '$email', '$id_pemilik'
                )";
                $result = mysqli_query($conn,$sql);
                if($result){
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $sql = "delete from catering where kd_cat = '$kd_cat'";
                        mysqli_query($conn,$sql);
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        echo json_encode($respon); exit();
                    }
                }else{
                    $respon['kode'] = "111";
                    echo json_encode($respon); exit();
                }
            }
        break;
        case "update":
            $kd_cat = $_POST['kd_cat'];
            $nama_cat = $_POST['nama_cat'];
            $nama_menu = $_POST['nama_menu'];
            $email = $_POST['email'];
            $nama_pemilik = $_POST['nama_pemilik'];
            $imstr = $_POST['image'];
            $file = $_POST['file'];
            $path = "images/catering/";

            $sql = "select m.id_menu, p.id_pemilik from menu m, pemilik p where m.nama_menu = '$nama_menu' and p.nama_pemilik = '$nama_pemilik'";
            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result)>0){
                $data = mysqli_fetch_assoc($result);
                $id_menu = $data['id_menu'];
                $id_pemilik = $data['id_pemilik'];

                $sql = "";
                if($imstr == ""){
                    $sql = "update catering set nama_cat='$nama_cat', id_menu='$id_menu', email='$email', id_pemilik='$id_pemilik' where kd_cat='$kd_cat'";
                    $result = mysqli_query($conn,$sql);
                    if($result){
                        echo json_encode($respon); exit();
                    }else{
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }
                }else{
                    if(file_put_contents($path.$file,base64_decode($imstr)) == false){
                        $respon['kode'] = "111";
                        echo json_encode($respon); exit();
                    }else{
                        $sql = "update catering set nama_cat='$nama_cat', id_menu='$id_menu', photosb='$file', email='$email', id_pemilik='$id_pemilik' where kd_cat='$kd_cat'";
                        $result = mysqli_query($conn,$sql);
                        if($result){
                            echo json_encode($respon); exit();
                        }else{
                            $respon['kode'] = "111";
                            echo json_encode($respon); exit();
                        }
                    }
                }
            }
        break;
        case "delete":
            $kd_cat = $_POST['kd_cat'];
            $sql = "select photosb from catering where kd_cat = '$kd_cat'";
            $result = mysqli_query($conn,$sql);
            if($result){
                if(mysqli_num_rows($result)>0){
                    $data = mysqli_fetch_assoc($result);
                    $photosb = $data['photosb'];
                    $path = "images/catering/";
                    unlink($path.$photosb);
                }
                $sql = "delete from catering where kd_cat= '$kd_cat'";
                $result = mysqli_query($conn,$sql);
                if($result){
                    echo json_encode($respon); exit();
                }else{
                    $respon['kode'] = '111';
                    echo json_encode($respon); exit();
                }
            }
        break;
    }
}