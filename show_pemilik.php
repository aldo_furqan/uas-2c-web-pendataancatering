<?php

$DB_NAME = "uas_catering";
$DB_USER = "root";
$DB_PASS = "";
$DB_SERVER_LOC = "localhost";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $conn = mysqli_connect($DB_SERVER_LOC, $DB_USER, $DB_PASS, $DB_NAME);
    $nama_pemilik = $_POST['nama_pemilik'];
    if(empty($nama_pemilik)){
        $sql = "SELECT *
            FROM pemilik 
            ORDER BY nama_pemilik ASC";
    }else{
        $sql = "SELECT *
            FROM pemilik
            WHERE nama_pemilik LIKE '%$nama_pemilik%' 
            ORDER BY nama_pemilik ASC";
    }
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json; charset=UTF-8");

        $data_pemilik = array();
        while ($pemilik = mysqli_fetch_assoc($result)) {
            array_push($data_pemilik, $pemilik);
        }
        echo json_encode($data_pemilik);
    }
}
