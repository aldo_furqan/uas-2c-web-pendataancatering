-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 18 Bulan Mei 2020 pada 07.26
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uas_catering`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `catering`
--

CREATE TABLE `catering` (
  `kd_cat` varchar(13) NOT NULL,
  `nama_cat` varchar(24) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `photosb` varchar(24) DEFAULT NULL,
  `email` varchar(24) DEFAULT NULL,
  `id_pemilik` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `catering`
--

INSERT INTO `catering` (`kd_cat`, `nama_cat`, `id_menu`, `photosb`, `email`, `id_pemilik`) VALUES
('C-0001', 'Surisma Catering', 1, 'DC_20200518103432.jpg', 'surisma@gmail.com', 6),
('C-0002', 'Anton Ct', 2, 'DC_20200518103807.jpg', 'antonct@gmail.com', 1),
('C-0003', 'Septa Ct baru', 6, 'DC_20200518103908.jpg', 'septa2@gmail.com', 5),
('C-0004', 'Indah Ct', 7, 'DC_20200518121532.jpg', 'indahbz@gmail.com', 7);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(30) DEFAULT NULL,
  `photosa` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id_menu`, `nama_menu`, `photosa`) VALUES
(1, 'Mie Bakso', 'bakso.jpg'),
(2, 'Mie Goreng', 'mie.jpg'),
(3, 'Nasi Goreng', 'nasgor.jpg'),
(6, 'Sate', 'DC20200518103345.jpg'),
(7, 'Tumpeng', 'DC20200518121416.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilik`
--

CREATE TABLE `pemilik` (
  `id_pemilik` int(11) NOT NULL,
  `nama_pemilik` varchar(30) DEFAULT NULL,
  `nohp` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pemilik`
--

INSERT INTO `pemilik` (`id_pemilik`, `nama_pemilik`, `nohp`) VALUES
(1, 'Anton', '081234551234'),
(5, 'Septa', '081212345432'),
(6, 'Surisma', '089878563456'),
(7, 'Indah ', '083423457621');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `catering`
--
ALTER TABLE `catering`
  ADD PRIMARY KEY (`kd_cat`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `pemilik`
--
ALTER TABLE `pemilik`
  ADD PRIMARY KEY (`id_pemilik`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `pemilik`
--
ALTER TABLE `pemilik`
  MODIFY `id_pemilik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
